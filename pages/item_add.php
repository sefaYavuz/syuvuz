<br />
<a href="/syuvuz/?page=groups_items_overview">&laquo; Terug naar het overzicht</a>
<h1>Item toevoegen</h1>

<?php
    if(isset($_POST['submit']))
    {
        $item = new Item();
        $item->setGroupId($_POST['group']);
        $item->setName($_POST['name']);
        if($item->insert($pdo)) {
            echo 'Het item is succesvol toegevoegd';
        }
    }
?>

<form action="" method="post">
    <div class="form-group">
        <label for="group">Groep</label>
        <select name="group" id="group">
            <?php
                $group = new Group();
                $group->getGroups($pdo, 'form');
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="name">Naam</label>
        <input type="text" name="name" id="name" required />
    </div>
    <div class="form-group">
        <input type="submit" name="submit" value="Opslaan" />
    </div>
</form>
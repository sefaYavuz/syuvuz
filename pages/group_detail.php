<?php
    try {
        $query = $pdo->prepare('SELECT * FROM groups WHERE id = :id');
        $query->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
        $query->setFetchMode(PDO::FETCH_CLASS, 'Group');
        $query->execute();
        $group = $query->fetch();
    } catch(PDOException $e) {
        echo 'Whoops, er iets fouts gegaan: ' . $e->getMessage();
    }
?>
<br />
<a href="/syuvuz/?page=groups_items_overview">&laquo; Terug naar het overzicht</a>
<h1>Groep - <?=$group->getName()?></h1>

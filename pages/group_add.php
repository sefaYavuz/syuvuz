<a href="/syuvuz/?page=groups_items_overview">&laquo; Terug naar het overzicht</a>
<h1>Groep toevoegen</h1>

<?php
    if(isset($_POST['submit']))
    {
        $group = new Group();
        $group->setName($_POST['name']);
        $group->setParentId(($_POST['parent_group'] != 0 ? $_POST['parent_group'] : null));
        if($group->insert($pdo)) {
            echo 'De groep is succesvol toegevoegd';
        }
    }
?>

<form action="" method="post">
    <div class="form-group">
        <label for="parent_group">Hoofdgroep</label>
        <select name="parent_group" id="parent_group">
            <option value="0">Geen</option>
            <?php
                $group = new Group();
                $group->getGroups($pdo, 'form');
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="name">Naam</label>
        <input type="text" name="name" id="name" required />
    </div>
    <div class="form-group">
        <input type="submit" name="submit" value="Opslaan" />
    </div>
</form>
<?php
try {
    $query = $pdo->prepare('SELECT name FROM items WHERE id = :id');
    $query->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
    $query->setFetchMode(PDO::FETCH_CLASS, 'Item');
    $query->execute();
    $item = $query->fetch();
} catch(PDOException $e) {
    echo 'Whoops, er iets fouts gegaan: ' . $e->getMessage();
}
?>
<br />
<a href="/syuvuz/?page=groups_items_overview">&laquo; Terug naar het overzicht</a>
<h1>Item - <?=$item->getName()?></h1>

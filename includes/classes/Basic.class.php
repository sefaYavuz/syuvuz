<?php

class Basic
{
    /**
     * Get pages
     *
     * @param string $sPage
     * @param PDO $pdo
     */
    public function getPage($sPage, $pdo)
    {
        $sPagePath = $_SERVER['DOCUMENT_ROOT'] . '/syuvuz/pages/' . $sPage . '.php';

        if (strpos($sPage, '.') !== false) {
            echo '403 | Geen toegang.';
        }
        else if (!file_exists($sPagePath)) {
            echo '404 | Pagina bestaat niet.';
        } else {
            include($sPagePath);
        }
    }
}
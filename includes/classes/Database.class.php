<?php

class Database {
    private $username = 'root';
    private $password = '';
    private $pdo;

    function __construct()
    {
        $this->setDbConn();
    }

    /**
     * Set the database connection
     *
     * @return string
     */
    public function setDbConn()
    {
        try {
            $this->pdo = new PDO('mysql:host=localhost;dbname=syavuz', $this->username, $this->password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            return 'Whoops, er is een error ontstaan: ' . $e->getMessage() . '<br />';
        }
    }

    /**
     * @return PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
}


<?php

class Group
{
    private $id;
    private $parent_id;
    private $parent_name;
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * @param int $parent_id
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;
    }

    /**
     * @return string
     */
    public function getParentName()
    {
        return $this->parent_name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Perform an insert query
     *
     * @param $pdo $pdo
     * @return bool
     */
    public function insert($pdo)
    {
        try {
            $query = $pdo->prepare("INSERT INTO groups (parent_id, name) VALUES (:parent_id, :name)");
            $query->bindParam(':parent_id', $this->getParentId(), PDO::PARAM_INT);
            $query->bindParam(':name', $this->getName(), PDO::PARAM_STR);
            if ($query->execute()) {
                return true;
            }
        } catch (PDOException $e) {
            echo 'Whoops, er is iets fout gegaan: ' . $e->getMessage();
        }

    }

    /**
     * Get all the groups using recursion
     *
     * @param $pdo
     * @param string $type
     * @param null $group
     * @param int $iSpaces
     */
    function getGroups($pdo, $type = 'overview', $group = null, $iSpaces = 0)
    {
        try {

            //add &nbsp; tags using str_repeat
            $sSpaces = str_repeat('&nbsp; ', $iSpaces);

            //check if the given group is not null, so we can know that it has a parent group
            if ($group != null) {
                $groupsQuery = $pdo->prepare('SELECT * FROM groups WHERE `parent_id` = :group_id');
                $groupsQuery->bindParam(':group_id', $group->getId(), PDO::PARAM_INT);
            } else {
                $groupsQuery = $pdo->prepare('SELECT * FROM groups WHERE parent_id IS NULL');
                $groupsQuery->setFetchMode(PDO::FETCH_CLASS, 'Group');
            }

            $groupsQuery->setFetchMode(PDO::FETCH_CLASS, 'Group');
            $groupsQuery->execute();

            if ($groupsQuery->rowCount() > 0) {
                foreach ($groupsQuery->fetchAll() as $group) {
                    //check if the type is for the form or the overview
                    if ($type == 'form') {
                        echo '<option value="' . $group->getId() . '">' . $sSpaces . $group->getName() . '</option>';

                        //go in recursion
                        $this->getGroups($pdo, $type, $group, $iSpaces + 1);
                    } else {
                        echo '<div class="container">';
                        echo '<h3><a href="/syuvuz/?page=group_detail&id=' . $group->getId() . '">' . $group->getName() . '</a></h3>';

                        //initialize a new Item object and get all items and go in recursion for groups
                        $item = new Item();
                        $item->getItems($pdo, $group);

                        $this->getGroups($pdo, $type, $group);

                        echo '</div>';
                    }
                }
            }
        } catch (PDOException $e) {
            echo 'Whoops, er iets fouts gegaan: ' . $e->getMessage();
        }
    }
}
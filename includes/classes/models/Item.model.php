<?php

class Item
{
    private $id;
    private $group_id;
    private $group_name;
    private $name;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * @param int $group_id
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * @return string
     */
    public function getGroupName()
    {
        return $this->group_name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Perform an insert query
     *
     * @param $pdo $pdo
     * @return bool
     */
    public function insert($pdo)
    {
        try {
            $query = $pdo->prepare("INSERT INTO items (group_id, name) VALUES (:group_id, :name)");
            $query->bindParam(':group_id', $this->getGroupId(), PDO::PARAM_INT);
            $query->bindParam(':name', $this->getName(), PDO::PARAM_STR);
            if ($query->execute()) {
                return true;
            }
        } catch (PDOException $e) {
            echo 'Whoops, er is iets fout gegaan: ' . $e->getMessage();
        }
    }

    /**
     * Get items for the overview
     *
     * @param $pdo
     * @param $group
     */
    function getItems($pdo, $group)
    {
        try {
            $itemsQuery = $pdo->prepare('SELECT * FROM items WHERE `group_id` = :group_id');
            $itemsQuery->bindParam(':group_id', $group->getId(), PDO::PARAM_INT);
            $itemsQuery->setFetchMode(PDO::FETCH_CLASS, 'Item');
            $itemsQuery->execute();

            if ($itemsQuery->rowCount() > 0) {
                foreach ($itemsQuery->fetchAll() as $item) {
                    echo '<h4><a href="/syuvuz/?page=item_detail&id='.$item->getId().'">' . $item->getName() . '</a></h4>';
                }
            }
        } catch (PDOException $e) {
            echo 'Whoops, er iets fouts gegaan: ' . $e->getMessage();
        }
    }
}
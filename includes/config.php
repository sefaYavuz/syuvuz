<?php

//autoloader function to autoload all classes
function classesAutoloader($class) {
    if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/syuvuz/includes/classes/' . $class . '.class.php'))
    {
        include $_SERVER['DOCUMENT_ROOT'] . '/syuvuz/includes/classes/' . $class . '.class.php';
    }
}

//autoloader function to autoload all model classes
function modelsAutoloader($class) {
    if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/syuvuz/includes/classes/models/' . $class . '.model.php'))
    {
        include $_SERVER['DOCUMENT_ROOT'] . '/syuvuz/includes/classes/models/' . $class . '.model.php';
    }
}

spl_autoload_register('modelsAutoloader');
spl_autoload_register('classesAutoloader');

//initialize standard classes
$basic = new Basic();

//set db connection and get pdo object
$database   = new Database();
$pdo       = $database->getPdo();

<?php
    require $_SERVER['DOCUMENT_ROOT'] . '/syuvuz/includes/config.php';

    $sPage = '';

    if(isset($_GET['page']) && !empty($_GET['page'])) {
       $sPage = $_GET['page'];
    }
    else
    {
        $sPage = 'home';
    }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Simple App - <?=ucfirst($sPage)?></title>
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
    <ul>
        <li class="<?=($sPage == 'home' ? 'active' : '')?>"><a href="/syuvuz">Home</a></li>
        <li class="<?=($sPage == 'groups_items_overview' ? 'active' : '')?>"><a href="/syuvuz?page=groups_items_overview">Groepen & Items</a></li>
    </ul>
    <?php
        $basic->getPage((isset($_GET['page']) && !empty($_GET['page']) ? $_GET['page'] : 'home'), $pdo);
    ?>
</body>
</html>